import axios from 'axios'
import { baseUrl } from '@/appConfig'

export async function getCountOrdrs () {
  const url = baseUrl + '/count_orders/'
  const response = await axios.get(url)
  const data = response.data
  return data
}

export async function getListOrdrs () {
  const url = baseUrl + '/list_all_orders/'
  const response = await axios.get(url)
  const data = response.data
  return data
}

export async function orderDetails (orderId: any) {
  const url = baseUrl + '/order_details/' + orderId
  const response = await axios.post(url)
  const data = response.data
  return data
}
